#! /bin/sh

# hybserv	Start/stop Hybserv 2 IRC Services
# Version:	hybserv  1.8.0  17-Mar-2003  joshk@triplehelix.org
# Version:	hybserv  1.9.2  05-Sep-2006  ag@roxor.cx

### BEGIN INIT INFO
# Provides:          hybserv
# Required-Start:    $remote_fs $syslog ircd-hybrid
# Required-Stop:     $remote_fs $syslog ircd-hybrid
# Should-Start:      $local_fs $network $named
# Should-Stop:       $local_fs $network $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Hybserv daemon init.d script
# Description:       Use to manage the Hybserv daemon.
### END INIT INFO

PATH=/sbin:/bin:/usr/sbin:/usr/bin

PATH=/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/sbin/hybserv
NAME=hybserv
DESC="Hybserv 2 IRC Services"

. /lib/lsb/init-functions

test -f $DAEMON || exit 0

set -e

if [ "$1" = "start" ] || [ "$1" = "restart" ] || [ "$1" = "force-reload" ]; then
	if [ ! `grep '#NOT-EDITED#' /etc/hybserv/hybserv.conf | wc -l` -eq 0 ]; then
		echo "Not starting $DESC: please edit /etc/hybserv/hybserv.conf first..."
		exit 0
	fi
fi

case "$1" in
  start)
	echo -n "Starting $DESC: $NAME"
	if [ -f "/var/run/ircd/$NAME.pid" ]; then
		echo " already running, if this is not the case, then call 'stop' first."
	else
		start-stop-daemon --start --quiet \
			-u irc -c irc --exec $DAEMON >/dev/null 2>&1
	fi
	echo "."
	;;
  stop)
	echo -n "Stopping $DESC: $NAME"
	if [ -f "/var/run/ircd/$NAME.pid" ]; then
		start-stop-daemon -K -p /var/run/ircd/hybserv.pid -s 15 -q --oknodo
		rm -f /var/run/ircd/hybserv.pid
	else
		echo " ... not running!" # This is abnormal, please file a bug report.
	fi

	echo "."
	;;
  reload)
  	if [ -f "/var/run/ircd/$NAME.pid" ]; then
  		echo -n "Reloading configuration files for $NAME..."
		kill -HUP `cat /var/run/ircd/$NAME.pid`
		echo "done."
	else
		echo "Not reloading configuration files for $NAME - not running!"
	fi
	;;
  restart|force-reload)
	echo -n "Restarting $DESC: $NAME"

	if [ -f "/var/run/ircd/$NAME.pid" ]; then
		start-stop-daemon -K -p /var/run/ircd/$NAME.pid -s 15 -q --oknodo
		sleep 1
	fi
	start-stop-daemon --start --quiet \
		-u irc -c irc --exec $DAEMON \
		>/dev/null 2>&1
	echo "."
	;;
  *)
	echo "Usage: $0 {start|stop|restart|reload|force-reload}" >&2
	exit 1
	;;
esac

exit 0
